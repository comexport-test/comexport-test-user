package com.example.demo.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.util.ReflectionTestUtils;

import com.example.demo.dto.UserCreateDto;
import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserFilterDto;
import com.example.demo.dto.UserUpdateDto;
import com.example.demo.exception.ApiException;
import com.example.demo.model.Reputation;
import com.example.demo.model.User;
import com.example.demo.repository.ReputationRepository;
import com.example.demo.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	
	@InjectMocks
	UserService userService;
	
	@SuppressWarnings("unchecked")
	@Test
	public void testFindAll_Empty() {
		UserFilterDto dto = new UserFilterDto();
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository mock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", mock);
		
		Mockito.when(mock.findAll(any(Example.class), any(Pageable.class))).thenReturn(Page.empty());
		
		List<UserDto> users = userService.find(dto);
		assertTrue(users.size() == 0);
	}
	
	@Test
	public void testFindById_Empty() {
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository mock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", mock);
		
		Mockito.when(mock.findByIdUserAndEnabledIsTrue(anyLong())).thenReturn(Optional.empty());
		
		Optional<UserDto> result = userService.find(1L);
		assertTrue(!result.isPresent());
	}
	
	@Test
	public void testCreateUser_Success() throws ApiException {
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository mock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", mock);
		
		UserCreateDto dto = new UserCreateDto();
		
		User result = new User();
		result.setIdUser(1L);
		
		Mockito.when(mock.save(any(User.class))).thenReturn(result);
		
		Long idUser = userService.createUser(dto);
		assertTrue(idUser == 1L);
	}
	
	@Test
	public void testUpdateUser_Success() throws ApiException {
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository mock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", mock);
		
		UserUpdateDto dto = new UserUpdateDto();
		
		User result = new User();
		result.setIdUser(1L);
		
		Mockito.when(mock.findById(anyLong())).thenReturn(Optional.of(result));
		Mockito.when(mock.save(any(User.class))).thenReturn(result);
		
		userService.updateUser(dto, 1L);
	}
	
	@Test
	public void testRemoveUser_Success() throws ApiException {
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository mock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", mock);
		
		User result = new User();
		result.setIdUser(1L);
		
		Mockito.when(mock.findById(anyLong())).thenReturn(Optional.of(result));
		Mockito.when(mock.save(any(User.class))).thenReturn(result);
		
		userService.removeUser(1L);
	}
	
	@Test
	public void testScoreUser_Success() throws ApiException {
		ReflectionTestUtils.setField(userService, "modelMapper", new ModelMapper());
		
		UserRepository userRepoMock = mock(UserRepository.class);
		ReflectionTestUtils.setField(userService, "userRepository", userRepoMock);
		
		ReputationRepository reputationRepoMock = mock(ReputationRepository.class);
		ReflectionTestUtils.setField(userService, "reputationRepository", reputationRepoMock);
		
		User result = new User();
		result.setReputation(new Reputation());
		result.setIdUser(1L);
		result.getReputation().setScore(0L);
		
		Mockito.when(userRepoMock.findById(anyLong())).thenReturn(Optional.of(result));
		Mockito.when(reputationRepoMock.save(any(Reputation.class))).thenReturn(result.getReputation());
		
		userService.scoreUser(1L, 10L);
	}
	
}
