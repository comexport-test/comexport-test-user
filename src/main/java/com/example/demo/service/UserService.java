package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dto.RoleCreateDto;
import com.example.demo.dto.UserCreateDto;
import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserFilterDto;
import com.example.demo.dto.UserUpdateDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;
import com.example.demo.model.Reputation;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.ReputationRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private ReputationRepository reputationRepository;
	
	private ModelMapper modelMapper;

	@Autowired
	public UserService(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	public List<UserDto> find(UserFilterDto filter) {
		User userExample = convertToEntity(filter);

		List<UserDto> result = new ArrayList<UserDto>();

		int page = filter.getPage() != null ? filter.getPage() : 0;
		int size = filter.getSize() != null && filter.getSize() < 1000 ? filter.getSize() : 1000;
		Pageable basePage = PageRequest.of(page, size);

		ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAll()
				.withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("email", ExampleMatcher.GenericPropertyMatchers.exact())
				.withMatcher("birthDate", ExampleMatcher.GenericPropertyMatchers.exact());

		Example<User> example = Example.of(userExample, customExampleMatcher);
		Page<User> queryResult = userRepository.findAll(example, basePage); 
		queryResult.map(user -> result.add(convertToDto(user)));
		
		return result;
	}

	public Optional<UserDto> find(Long idUser) {
		return userRepository.findByIdUserAndEnabledIsTrue(idUser).map(user -> convertToDto(user));
	}
	
	@Transactional
	public Long createUser(UserCreateDto dto) throws ApiException {
		User user = convertToEntity(dto);

		LocalDateTime now = LocalDateTime.now();
		user.setUpdatedAt(now);
		user.setCreatedAt(now);
		user.setEnabled(true);

		user.setRole(validateRole(dto.getRole()));
		user.setReputation(buildReputation(user));
		user = userRepository.save(user);
		//reputationRepository.save();
		
		return user.getIdUser();
	}

	@Transactional
	public void updateUser(@Valid UserUpdateDto dto, Long id) throws ApiException {
		User original = userRepository.findById(id)
				.orElseThrow(() -> new ApiException(User.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));

		original.setBirthDate(dto.getBirthDate());
		original.setName(dto.getName());
		original.setUpdatedAt(LocalDateTime.now());
		
		original.setRole(validateRole(dto.getRole()));

		userRepository.save(original);
	}

	public void removeUser(@Valid Long id) throws ApiException {
		User original = userRepository.findById(id)
				.orElseThrow(() -> new ApiException(User.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));

		original.setEnabled(false);
		userRepository.save(original);
	}
	
	public Long scoreUser(Long id, Long score) throws ApiException {
		User original = userRepository.findById(id)
				.orElseThrow(() -> new ApiException(User.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));
		
		Reputation rep = original.getReputation();
		rep.setScore(original.getReputation().getScore() + score);
		rep.setUpdatedAt(LocalDateTime.now());
		reputationRepository.save(rep);
		return rep.getScore();
	}
	
	private Reputation buildReputation(User user) {
		Reputation rep = new Reputation();
		LocalDateTime now = LocalDateTime.now();
		rep.setCreatedAt(now);
		rep.setUpdatedAt(now);
		//rep.setIdUser(user.getIdUser());
		rep.setScore(0L);
		rep.setUser(user);
		return rep;
	}

	private UserDto convertToDto(User user) {
		UserDto dto = modelMapper.map(user, UserDto.class);
		return dto;
	}

	private User convertToEntity(UserCreateDto dto) {
		User result = modelMapper.map(dto, User.class);
		return result;
	}

	private User convertToEntity(UserFilterDto dto) {
		User result = modelMapper.map(dto, User.class);
		return result;
	}

	private Role validateRole(RoleCreateDto roleCreateDto) throws ApiException {
		if (roleCreateDto != null) {
			if (roleCreateDto.getId() != null) {
				Role role = roleRepository.findById(roleCreateDto.getId())
						.orElseThrow(() -> new ApiException(Role.class.getSimpleName(),
								roleCreateDto.getId().toString(), ExceptionType.NOT_FOUND));

				if (!role.isEnabled()) {
					throw new ApiException(Role.class.getSimpleName(), roleCreateDto.getId().toString(),
							ExceptionType.NOT_ACTIVE);
				}

				return role;
			}

			if (roleCreateDto.getDescription() != null) {
				Role role = new Role();
				role.setDescription(roleCreateDto.getDescription());
				role.setCreatedAt(LocalDateTime.now());
				role.setUpdatedAt(LocalDateTime.now());
				return role;
			}
		}
		return null;
	}

	

}
