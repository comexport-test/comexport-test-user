package com.example.demo.model.mapped;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public class AliveActive extends Active {
	
	@NotNull
	private LocalDateTime createdAt;
	
	@NotNull
	private LocalDateTime updatedAt;
	
}
