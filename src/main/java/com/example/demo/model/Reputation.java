package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.example.demo.model.mapped.Alive;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@Entity
@Table(name = "reputation_user")
public class Reputation extends Alive {
	
	@Id
	Long idUser;
	
	@JoinColumn(name = "id_user")
    @OneToOne
    @MapsId
    private User user;
	
	Long score;
}
