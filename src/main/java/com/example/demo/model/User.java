package com.example.demo.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.example.demo.model.mapped.AliveActive;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@Entity
public class User extends AliveActive {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long idUser;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_role")
	Role role;
	
	@OneToOne(fetch = FetchType.EAGER, mappedBy="user", cascade = CascadeType.ALL, optional = false, orphanRemoval = true)
	Reputation reputation;
	
	@NotNull
	String name;
	
	@NotNull
	String email;
	
	LocalDate birthDate;
}
