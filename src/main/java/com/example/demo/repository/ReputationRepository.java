package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Reputation;

public interface ReputationRepository extends JpaRepository<Reputation, Long> {
	
}
