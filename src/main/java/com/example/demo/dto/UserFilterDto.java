package com.example.demo.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Data;

@Data
public class UserFilterDto {
	Integer page;
	Integer size;
	String email;
	String name;
	
	@DateTimeFormat(iso = ISO.DATE)
	LocalDate birthDate;

}
