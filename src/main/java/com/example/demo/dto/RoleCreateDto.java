package com.example.demo.dto;

import lombok.Data;

@Data
public class RoleCreateDto {
	
	Long id;
	String description;
	
}
