package com.example.demo.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserCreateDto {
	
	RoleCreateDto role;

	@NotNull
	String name;
	
	@NotNull
	String email;
	
	@NotNull
	LocalDate birthDate;
}
