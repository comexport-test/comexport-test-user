package com.example.demo.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class UserDto {
	
	Long id;
	
	@NotNull
	ReputationDto reputation;
	
	RoleDto role;

	@NotNull
	String name;
	
	@NotNull
	String email;
	
	@NotNull
	LocalDate birthDate;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
