package com.example.demo.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserUpdateDto {
	
	@NotNull
	String name;
	
	@NotNull
	LocalDate birthDate;
	
	RoleCreateDto role;
}
