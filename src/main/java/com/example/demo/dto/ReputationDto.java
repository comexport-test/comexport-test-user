package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class ReputationDto {
	
	Long idUser;
	Long score;
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
