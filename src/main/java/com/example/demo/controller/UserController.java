package com.example.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserCreateDto;
import com.example.demo.dto.UserDto;
import com.example.demo.dto.UserFilterDto;
import com.example.demo.dto.UserUpdateDto;
import com.example.demo.exception.ApiException;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/api/user")
class UserController {
	
	//TODO Reputation
	
	private final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@GetMapping
	Collection<UserDto> users(UserFilterDto filter) {
		return userService.find(filter);
	}

	@GetMapping("/{id}")
	ResponseEntity<?> user(@PathVariable Long id) {
		
		Optional<UserDto> result = userService.find(id);
		return result.map(response -> ResponseEntity.ok().body(response)).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	ResponseEntity<ResultDto> createUser(
			@Valid @RequestBody UserCreateDto dto
			) throws URISyntaxException, ApiException {

		log.info("Request to create user: ", dto);
		Long idUser = userService.createUser(dto);
		
		return ResponseEntity.created(new URI("/api/user/" + idUser))
				.body(new ResultDto(idUser, HttpStatus.CREATED));
	}

	@PutMapping("/{id}")
	ResponseEntity<UserUpdateDto> updateUser(
			@PathVariable Long id, 
			@Valid @RequestBody UserUpdateDto dto
			) throws ApiException {
		
		log.info("Request to update user: ", dto);
		userService.updateUser(dto, id);
		return ResponseEntity.ok().body(dto);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> removerPessoa(@PathVariable Long id) throws ApiException {
		
		log.info("Request to delete user: ", id);
		userService.removeUser(id);
		return ResponseEntity.ok().body("User removed: " + id);
	}
	
	@PatchMapping("/{id}/score/{score}")
	public ResponseEntity<String> removerPessoa(@PathVariable Long id, @PathVariable Long score) throws ApiException {
		
		log.info("Request to score user: ", id);
		Long newScore = userService.scoreUser(id, score); 
		
		return ResponseEntity.ok().body("User scored: " + id + ", new score: " + newScore);
	}

}